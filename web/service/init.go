package service

import (
	"github.com/urfave/cli"
	"github.com/wi-cuckoo/eastlake/web/service/auth"
	"github.com/wi-cuckoo/eastlake/web/service/store"
)

var (
	Gitlab auth.Provider
	Orm    store.Store
)

// Init service
func InitService(c *cli.Context) {
	GitlabTmp := auth.New(
		c.String("gitlabServer"),
		c.String("gitlabClientID"),
		c.String("gitlabClientSecret"),
	)
	OrmTmp := store.New(
		c.String("sqlite"),
		c.Bool("debug"),
	)
	Gitlab = GitlabTmp
	Orm = OrmTmp
}
