package store

import (
	"github.com/wi-cuckoo/eastlake/web/model"
	"gorm.io/gorm"
)

// Store ...
type Store interface {
	// user about
	QueryUsers(opts *model.UserQueryOpts) (int64, []*model.User, error)
	UserInfo(uid int) (*model.User, error)
	UserInfoByUsername(username string) (*model.User, error)
	CreateUser(u *model.User) error
	UpdateUser(uid int, u *model.User) error

	// gorm transaction
	GetTransaction() *gorm.DB
}
