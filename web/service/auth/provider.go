package auth

import (
	"net/http"
	"github.com/wi-cuckoo/eastlake/web/model"
)

// Provider ...
type Provider interface {
	// Login to Oath2 Server
	Login(w http.ResponseWriter, r *http.Request) (*model.User, error)
	// Logout from server
	Logout(w http.ResponseWriter, r *http.Request) error
}
