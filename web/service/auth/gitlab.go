package auth

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"mime"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/wi-cuckoo/eastlake/web/model"
	"github.com/wi-cuckoo/eastlake/util"
)

// Gitlab ...
type Gitlab struct {
	URL          string
	ClientID     string
	ClientSecret string
	AuthURL      string
	TokenURL     string
	RedirectURL  string
	ProfileURL   string

	tr http.RoundTripper
}

// New ...
func New(addr, id, secret string) Provider {
	u, err := url.Parse(addr)
	if err != nil {
		panic(err.Error())
	}
	host, _, err := net.SplitHostPort(u.Host)
	if err == nil {
		u.Host = host
	}
	trans := &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true,
		},
	}
	addr = u.Scheme + "://" + u.Host
	return &Gitlab{
		URL:          addr,
		ClientID:     id,
		ClientSecret: secret,
		AuthURL:      fmt.Sprintf("%s/oauth/authorize", addr),
		TokenURL:     fmt.Sprintf("%s/oauth/token", addr),
		ProfileURL:   fmt.Sprintf("%s/api/v3/user", addr),
		tr:           trans,
	}
}

// Login impliment
func (o *Gitlab) Login(w http.ResponseWriter, r *http.Request) (*model.User, error) {
	o.RedirectURL = fmt.Sprintf("%s/login", util.GetURL(r))
	code := r.FormValue("code")
	if code == "" {
		http.Redirect(w, r, o.authCodeURL(), http.StatusSeeOther)
		return nil, nil
	}

	token, err := o.exchange(code)
	if err != nil {
		return nil, err
	}

	return o.profile(token.AccessToken)
}

// Logout impliment
func (o *Gitlab) Logout(w http.ResponseWriter, r *http.Request) error {
	return nil
}

func (o *Gitlab) authCodeURL() string {
	u, _ := url.Parse(o.AuthURL)
	q := url.Values{
		"response_type": {"code"},
		"client_id":     {o.ClientID},
		"redirect_uri":  {o.RedirectURL},
	}.Encode()
	if u.RawQuery == "" {
		u.RawQuery = q
	} else {
		u.RawQuery += "&" + q
	}
	return u.String()
}

func (o *Gitlab) profile(token string) (*model.User, error) {
	config := &tls.Config{InsecureSkipVerify: true}
	tr := &http.Transport{
		Proxy:           http.ProxyFromEnvironment,
		TLSClientConfig: config,
	}
	client := &http.Client{Transport: tr}
	qs := url.Values{
		"access_token": {token},
	}.Encode()
	req, err := http.NewRequest("GET", o.ProfileURL+"?"+qs, nil)
	if err != nil {
		return nil, err
	}
	r, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	var user struct {
		ID         int    `json:"id"`
		Name       string `json:"name"`
		Un         string `json:"username"`
		Avatar     string `json:"avatar_url"`
		Email      string `json:"email"`
		Identities []struct {
			ExternUID string `json:"extern_uid"`
		} `json:"identities"`
	}
	if err := json.Unmarshal(body, &user); err != nil {
		return nil, err
	}

	uid := 0
	if len(user.Identities) > 0 {
		uid, _ = strconv.Atoi(user.Identities[0].ExternUID)
	}

	return &model.User{
		UID:      uid,
		GID:      user.ID,
		Email:    user.Email,
		Username: user.Un,
		Nickname: user.Name,
		Avatar:   user.Avatar,
		Token:    token,
	}, nil
}

// Token ...
type Token struct {
	AccessToken string
	ExpiresIn   time.Time
}

// Exchange code -> token
func (o *Gitlab) exchange(code string) (*Token, error) {
	qs := url.Values{
		"grant_type":    {"authorization_code"},
		"scope":         {"api"},
		"code":          {code},
		"redirect_uri":  {o.RedirectURL},
		"client_id":     {o.ClientID},
		"client_secret": {o.ClientSecret},
	}.Encode()
	client := &http.Client{Transport: o.tr}
	req, err := http.NewRequest("POST", o.TokenURL, strings.NewReader(qs))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.SetBasicAuth(o.ClientID, o.ClientSecret)
	r, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()
	if r.StatusCode != 200 {
		return nil, errors.New("unexpected status " + r.Status)
	}

	var b struct {
		Access    string `json:"access_token"`
		Refresh   string `json:"refresh_token"`
		ExpiresIn int64  `json:"expires_in"` // seconds
		ID        string `json:"id_token"`
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}
	content, _, _ := mime.ParseMediaType(r.Header.Get("Content-Type"))
	switch content {
	case "application/x-www-form-urlencoded", "text/plain":
		vals, err := url.ParseQuery(string(body))
		if err != nil {
			return nil, err
		}

		b.Access = vals.Get("access_token")
		b.ExpiresIn, _ = strconv.ParseInt(vals.Get("expires_in"), 10, 64)
	default:
		if err = json.Unmarshal(body, &b); err != nil {
			return nil, fmt.Errorf("got bad response from server: %q", body)
		}
	}

	if b.Access == "" {
		return nil, errors.New("got empty token")
	}
	if b.ExpiresIn <= 0 {
		b.ExpiresIn = 7200
	}

	return &Token{
		AccessToken: b.Access,
		ExpiresIn:   time.Now().Add(time.Duration(b.ExpiresIn) * time.Second),
	}, nil
}
