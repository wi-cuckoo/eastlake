package web

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

var corsOpts = cors.Options{
	AllowedOrigins:   []string{"*"},
	AllowedMethods:   []string{"GET", "POST", "PATCH", "PUT", "DELETE", "OPTIONS"},
	AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
	ExposedHeaders:   []string{"Link"},
	AllowCredentials: true,
	MaxAge:           300,
}

// Handler  web api handler
func (s *Server) Handler() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.Recoverer)
	r.Use(middleware.NoCache)

	cors := cors.New(corsOpts)
	r.Use(cors.Handler)

	r.Route("/", func(r chi.Router) {
		r.Get("/", s.homePage())
		r.Post("/hook", s.demo)
	})
	r.Route("/trigger", func(r chi.Router) {
		r.Post("/demo", s.demo)
	})
	r.Route("/ping", func(r chi.Router) {
		r.Get("/", s.handlePing())
	})
	r.Route("/login", func(r chi.Router) {
		r.Get("/", s.login())
	})
	r.Route("/logout", func(r chi.Router) {
		r.Get("/", s.logout())
	})
	r.Route("/userinfo", func(r chi.Router) {
		r.Get("/", s.UserInfo())
	})
	return r
}
