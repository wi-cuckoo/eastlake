package model

type PaginationOpts struct {
	Size int `form:"pageSize" binding:"gte=1,lte=1000"`
	Page int `form:"current" binding:"gte=1"`
}
