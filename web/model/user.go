package model

// User ...
type User struct {
	UID        int     `json:"uid" gorm:"column:uid"`
	GID        int     `json:"gid" gorm:"column:gid"`
	Username   string  `json:"username" gorm:"column:username;primary_key"`
	Nickname   string  `json:"nickname" gorm:"column:nickname"`
	Email      string  `json:"email" gorm:"column:email"`
	Token      string  `json:"-" gorm:"column:token"`
	Secret     string  `json:"-" gorm:"column:secret"`
	Avatar     string  `json:"avatar" gorm:"column:avatar"`
	Hash       string  `json:"-" gorm:"column:hash"`
	CreateTime int64   `json:"create_time" gorm:"column:create_time"`
	UpdateTime int64   `json:"update_time" gorm:"column:update_time"`
}

// TableName ....
func (t *User) TableName() string {
	return "user"
}

// UserQueryOpts ...
type UserQueryOpts struct {
	PaginationOpts
	Key       string   `form:"key"`
	Username  string   `form:"username"`
	Nickname  string   `form:"nickname"`
	Usernames []string `form:"usernames"`
}

