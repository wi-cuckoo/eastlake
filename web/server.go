package web

// Server to serve web api
type Server struct {
	usage string
}

// NewServer create a server
func NewServer(usage string) *Server {
	return &Server{
		usage: usage,
	}
}
