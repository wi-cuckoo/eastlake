## Eastlake

Eastlake 是一个以云原生理念设计，并充分结合企业生产实际情况设计的CI/CD解决方案。Eastlake具有如下特性：

- 采用master/worker分层架构，任务调度与构建分离，可水平扩展CICD能力
- 依赖极少，全部流程采用容器编排方式，容器即插件
- **Clean Environment**，应用之间，单应用各CICD阶段之间环境隔离，用完即毁

## Pipeline设计

Eastlake的Pipeline设计充分体现两个理念：1. 配置即代码；2. 容器即插件

### 配置即代码

首先，我们将Pipeline设计分为多stage，每个stage又可以由多个step组成。简单理解的话，通过一次pipeline流程，可以进行多次的CI/CD。

然后我们结合公司的业务实际，会预先将这些pipeline定制起来，看起来就像这样：
```yml
kind: pipeline
name: default

steps:
- name: test
  image: golang:latest
  volumes:
  - name: deps
    path: /go
  commands:
  - make test

- name: build
  image: golang:latest
  volumes:
  - name: deps
    path: /go
  commands:
  - make build

- name: publish
  image: publish:v1.0
  volumes:
  - name: deps
    path: /go
  - name: dockersock
    path: /var/run/docker.sock
  commands:
  - make publish

- name: deploy
  image: deploy:t1.0
  commands:
  - make deploy

volumes:
- name: deps
  temp: {}
- name: dockersock
  host:
    path: /var/run/docker.sock

```

通过这样的定制，应用在发布的时候只需要选择对应的pipeline模板，辅以一些必要的参数，就可以完成一次发布。

**并且作为平台，我们并不过多约束应用的CI/CD行为**，因此在践行Devops的时候，我们将每个Step设计为以Makefile
方式进行构建，通过约定的make执行而不是直接执行的方式，我们希望平台给研发人员提供更大的灵活性。

### 容器即插件

传统的Pipeline通过命令编排，执行一连串必要的shell命令来达到CI/CD的目的，这给环境运维带来了极大的复杂性和困难。而身处上云上容器
的大时代，Eastlake的设计上果断采用了容器编排来取代直接的命令编排方式，简单理解就是，**我们将每一个step都放到了容器去执行**。这样
的好处也是显而易见的：

- 每一个步骤都是环境隔离的，不会相互影响
- 宿主机的维护十分简单，只需要一个容器环境
- 通过定制容器，我们可以天然获得插件的能力

## Getting Started

环境依赖：
  - docker 17.02+
  - minikube latest

1. 设置环境变量

```
DRONE_GITLAB_SKIP_VERIFY=true
DRONE_RPC_SERVER=http://local.firefly.com:4399
DRONE_GITLAB_DEBUG=true
DRONE_RPC_SECRET=ojbk
DRONE_GITLAB_CLIENT_ID=c9189489800347df31af5fc438a73836bad14807369f3e717bd9ae0fdf44c6b8
DRONE_GITLAB_CLIENT_SECRET=48ea978bbca36766073adf143523ab4b894c71dc4c17730dd18b27544d974c42
DRONE_SERVER_HOST=local.firefly.com:4399
DRONE_SERVER_PORT=:4399
DRONE_RUNNER_CAPACITY=1
DRONE_SERVER_PROTO=http
DRONE_LOGS_DEBUG=true
```
注：其中 local.firefly.com 为绑定本机的域名

2. 启动 server/agent

```
[root@local]# ./bin/eastlake
[root@local]# ./bin/agent 
```

3. 触发构建示例

```
[root@local]# curl -X POST --cookie="user:admin" http://local.firefly.com:4399/trigger/demo
```
