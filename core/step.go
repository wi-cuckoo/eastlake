package core

// Step execution
type Step struct {
	ID        int64  `json:"id"`
	StageID   int64  `json:"step_id"`
	Number    int    `json:"number"`
	Name      string `json:"name"`
	Status    string `json:"status"`
	Error     string `json:"error,omitempty"`
	ErrIgnore bool   `json:"errignore,omitempty"`
	ExitCode  int    `json:"exit_code"`
	Started   int64  `json:"started,omitempty"`
	Stopped   int64  `json:"stopped,omitempty"`
	Version   int64  `json:"version"`
}
