package core

// Perm represents an individuals repository
// permission.
type Perm struct {
	UserID  int64  `db:"perm_user_id"  json:"-"`
	RepoUID string `db:"perm_repo_uid" json:"-"`
	Read    bool   `db:"perm_read"     json:"read"`
	Write   bool   `db:"perm_write"    json:"write"`
	Admin   bool   `db:"perm_admin"    json:"admin"`
	Synced  int64  `db:"perm_synced"   json:"-"`
	Created int64  `db:"perm_created"  json:"-"`
	Updated int64  `db:"perm_updated"  json:"-"`
}
