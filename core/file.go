package core

// File represents the raw file contents in the remote
// version control system.
type File struct {
	Data []byte
	Hash []byte
}

// FileArgs provides repository and commit details required
// to fetch the file from the  remote source code management
// service.
type FileArgs struct {
	Commit string
	Ref    string
}
