package core

import "fmt"

// Netrc contains login and initialization information
type Netrc struct {
	Machine  string `json:"machine"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

// String returns the string representation of a netrc file.
func (n *Netrc) String() string {
	return fmt.Sprintf("machine %s login %s password %s",
		n.Machine,
		n.Login,
		n.Password,
	)
}
