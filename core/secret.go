package core

// Secret represents a secret variable, such as a password or token,
// that is provided to the build at runtime.
type Secret struct {
	ID              int64  `json:"id,omitempty"`
	RepoID          int64  `json:"repo_id,omitempty"`
	Namespace       string `json:"namespace,omitempty"`
	Name            string `json:"name,omitempty"`
	Type            string `json:"type,omitempty"`
	Data            string `json:"data,omitempty"`
	PullRequest     bool   `json:"pull_request,omitempty"`
	PullRequestPush bool   `json:"pull_request_push,omitempty"`
}
