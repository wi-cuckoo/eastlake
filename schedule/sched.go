package schedule

import "time"

var ch = make(chan int64, 100)

// Enqueue the stage id
func Enqueue(id int64) {
	ch <- id
}

// Dequeue get a stage id, -1 means timeout
func Dequeue() int64 {
	select {
	case id := <-ch:
		return id
	case <-time.After(time.Second * 30):
		return -1
	}
}
