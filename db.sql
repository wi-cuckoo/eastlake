-- Create Table for eastlake --

CREATE TABLE `user` (
  `uid` INTEGER (12) NOT NULL COMMENT '用户id',
  `gid` INTEGER (12) DEFAULT NULL COMMENT '用户组id',
  `username` VARCHAR (255) NOT NULL COMMENT '用户名',
  `nickname` VARCHAR (255) DEFAULT NULL COMMENT '用户别名',
  `email` VARCHAR (255) DEFAULT NULL COMMENT '邮箱',
  `token` VARCHAR (255) DEFAULT NULL COMMENT 'oauth token',
  `secret` VARCHAR (255) DEFAULT NULL COMMENT '密码',
  `avatar` VARCHAR (255) DEFAULT NULL COMMENT 'avatar',
  `hash` VARCHAR (255) DEFAULT NULL COMMENT 'hash',
  `create_time` INTEGER (14) DEFAULT NULL COMMENT '创建时间',
  `update_time` INTEGER (14) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARACTER SET = utf8mb4 COMMENT='用户表';
