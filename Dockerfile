FROM centos:7

EXPOSE 4399
COPY bin/eastlake /opt/eastlake

CMD ["/opt/eastlake"]
