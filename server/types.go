package server

import (
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/wi-cuckoo/eastlake/core"
)

type requestRequest struct {
	Request *Request `json:"request"`
}

type acceptRequest struct {
	Stage   int64
	Machine string
}

type netrcRequest struct {
	Repo int64
}

// Request body
type Request struct {
	Kind    string            `json:"kind"`
	Type    string            `json:"type"`
	OS      string            `json:"os"`
	Arch    string            `json:"arch"`
	Variant string            `json:"variant"`
	Kernel  string            `json:"kernel"`
	Labels  map[string]string `json:"labels,omitempty"`
}

type buildContextToken struct {
	Secret  string
	Context *Context
}

// Context represents the minimum amount of information
// required by the runner to execute a build.
type Context struct {
	Repo    *core.Repository `json:"repository"`
	Build   *core.Build      `json:"build"`
	Stage   *core.Stage      `json:"stage"`
	Config  *core.File       `json:"config"`
	Secrets []*core.Secret   `json:"secrets"`
	System  *core.System     `json:"system"`
}

const defaultConfig = `
kind: pipeline
name: default

steps:
- name: test
  image: golang
  volumes:
  - name: deps
    path: /go
  commands:
  - go test

- name: build
  image: golang
  volumes:
  - name: deps
    path: /go
  commands:
  - go build

- name: publish
  image: docker:dind
  volumes:
  - name: deps
    path: /go
  - name: dockersock
    path: /var/run/docker.sock
  commands:
  - docker build -t docker.io/test:v1 .

- name: deploy
  image: docker:dind
  volumes:
  - name: dockersock
    path: /var/run/docker.sock
  commands:
  - docker run -d -p 12345:12345 docker.io/test:v1

volumes:
- name: deps
  temp: {}
- name: dockersock
  host:
    path: /var/run/docker.sock
`

func getConfigData() []byte {
	buf, err := ioutil.ReadFile("./template/go.yaml")
	if err != nil {
		return []byte(strings.TrimSpace(defaultConfig))
	}
	return buf
}

func getBuildCommit() string {
	if sha := os.Getenv("BUILD_COMMIT_ID"); sha != "" {
		return sha
	}
	return "4128b064d59a56e0a16d6b89e9cab1b1667d75c4"
}

// For Demo
var (
	TestSecret = "grsem1Ck7vJoHrrSVhahzSfpvzL2eh8H"
	TestRepo   = &core.Repository{
		ID:         8,
		UID:        "2",
		Namespace:  "yuweiguo",
		Name:       "hello",
		Slug:       "yuweiguo/hello",
		HTTPURL:    "http://ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn:1080/yuweiguo/hello.git",
		SSHURL:     "ssh://git@ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn:1022/yuweiguo/hello.git",
		Link:       "http://ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn:1080/yuweiguo/hello",
		Branch:     "master",
		Private:    true,
		Visibility: "public",
		Trusted:    true,
		Active:     true,
		Timeout:    60,
		Counter:    2,
		Version:    5,
	}
	// stage from parsing .drone.yml
	TestStage = &core.Stage{
		ID:        110,
		RepoID:    8,
		BuildID:   1,
		Number:    5,
		Name:      "default",
		Kind:      "pipeline",
		Type:      "docker",
		Status:    "pending",
		Machine:   "ip-172-31-40-250.cn-northwest-1.compute.internal",
		OS:        "linux",
		Arch:      "amd64",
		Created:   time.Now().Unix(),
		Updated:   time.Now().Unix(),
		Version:   2,
		OnSuccess: true,
	}
	TestBuild = &core.Build{
		ID:      110,
		RepoID:  TestRepo.ID,
		Trigger: "@hook",
		Status:  TestStage.Status,
		Link:    "http://ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn:1080/yuweiguo/hello/-/commit/8aa6b241cee0bdc2b72e6810b36397cbcd55302c",
		Message: "Add new file",
		After:   getBuildCommit(),
		Ref:     "refs/heads/master",
		Source:  "master",
		Target:  "master",
		Version: 1,
		Stages:  []*core.Stage{TestStage},
	}
	TestConf = &core.File{
		Data: getConfigData(),
	}
	TestSys = &core.System{
		Proto:   "http",
		Host:    "ec2-161-189-8-59.cn-northwest-1.compute.amazonaws.com.cn:8000",
		Link:    "http://ec2-161-189-8-59.cn-northwest-1.compute.amazonaws.com.cn:8000",
		Version: "1.9.0",
	}
	TestContext = &Context{
		Repo:   TestRepo,
		Build:  TestBuild,
		Stage:  TestStage,
		Config: TestConf,
		System: TestSys,
	}
	TestNetrc = &core.Netrc{
		Machine:  "ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn",
		Login:    "oauth2",
		Password: "fd4781d862f4de5c353fdc986d70cc51fc932cf27506c89d3164778e41186442",
	}
)

// {"machine":"ec2-68-79-45-151.cn-northwest-1.compute.amazonaws.com.cn","login":"oauth2","password":"fd4781d862f4de5c353fdc986d70cc51fc932cf27506c89d3164778e41186442"}
