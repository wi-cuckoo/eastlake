module github.com/wi-cuckoo/eastlake

go 1.14

require (
	github.com/elastic/go-elasticsearch/v6 v6.8.10
	github.com/gin-gonic/gin v1.6.3
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/securecookie v1.1.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.0.0
	github.com/mattn/go-sqlite3 v1.14.3
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/objx v0.1.1 // indirect
	github.com/urfave/cli v1.22.4
	github.com/wi-cuckoo/goahead v0.0.0-20191218053951-0c52d87b2e4f // indirect
	go.mongodb.org/mongo-driver v1.4.2
	gorm.io/driver/mysql v1.0.2
	gorm.io/driver/sqlite v1.1.3
	gorm.io/gorm v1.20.2
)
